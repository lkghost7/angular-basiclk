import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';

export interface  IPost {
  title?: string
  text?: string
  id?: number
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnChanges{
  posts: IPost[] = [];

  ngOnInit(): void {
    console.log('onInit')

    setTimeout(()=> {
      console.log('timeout')
      this.posts[0].title = 'changed'
    }, 5000)


    this.posts =  [
      {title: 'angular components', text: 'I am hangry', id: 1},
      {title: 'angular components2', text: 'I am hangry2', id: 2},
      {title: 'angular components3', text: 'I am hangry3', id: 3}
    ]
  }


  updatePost(post: IPost ) {
    this.posts.unshift(post)
    console.log('Post', post)
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('ngonchanges')
  }

  removePost(id: number) {

    console.log('id to remove', id)
    this.posts = this.posts.filter(p=> p.id !== id)
  }
}
