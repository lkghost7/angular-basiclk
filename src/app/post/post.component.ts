import {
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewEncapsulation
} from '@angular/core';
import {IPost} from "../app.component";

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default,
  encapsulation: ViewEncapsulation.None
})
export class PostComponent implements OnInit{

  // @Input() post: Post
  @Input() post: IPost;
  @Output() onRemove = new EventEmitter<number>()
  @ContentChild('info', {static: true}) infoRef: ElementRef

  // constructor() {
  //   this.post = {} as IPost;
  // }

  ngOnInit(): void {
    console.log(this.infoRef.nativeElement)

  }

  removePost() {

    this.onRemove.emit(this.post.id)
  }
}
